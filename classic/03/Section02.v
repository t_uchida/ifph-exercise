Lemma plus_n_0 : forall n, n + 0 = n.
  intro n. induction n.
  - reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Theorem ex_3_2_1 : forall n, S O * n = n.
  intro n. induction n.
  - unfold mult. unfold plus. reflexivity.
  - unfold mult. apply plus_n_0.
Qed.

Theorem ex_3_2_2 : forall A (f : A -> A -> A) (a b : A),
    (forall x, f a x = x) -> (forall x, f x b = x) -> a = b.
  intros A f a b H0 H1. rewrite <- H0. rewrite H1. reflexivity.
Qed.

Fixpoint power (n m : nat) : nat :=
  match m with
    | 0 => 1
    | S p => n * power n p
  end.

Lemma plus_assoc : forall n m p : nat, n + (m + p) = n + m + p.
  intros n m p. induction n; simpl.
  - reflexivity.
  - rewrite IHn. reflexivity.
Qed.

Lemma plus_n_Sm : forall n m : nat, S (n + m) = n + S m.
  intros n m. induction n.
  - simpl. reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma plus_comm : forall n m : nat, n + m = m + n.
  intros n m. induction n.
  - simpl. symmetry. apply plus_n_0.
  - simpl. rewrite IHn. apply plus_n_Sm.
Qed.

Lemma mult_plus_distr_l : forall n m p : nat, n * (m + p) = n * m + n * p.
  intros n m p. induction n; simpl.
  - reflexivity.
  - rewrite IHn. rewrite <- (plus_assoc m p _). rewrite (plus_assoc p _ _).
    rewrite (plus_comm p (n * m)). rewrite <- (plus_assoc (n * m) _ _).
    rewrite (plus_assoc m _ _). reflexivity.
Qed.

Lemma mult_n_0 : forall n, n * 0 = 0.
  intro n. induction n.
  - simpl. reflexivity.
  - simpl. apply IHn.
Qed.

Lemma mult_n_Sm : forall n m : nat, n * S m = n + n * m.
  intros n m. induction n; simpl.
  - reflexivity.
  - rewrite IHn. rewrite plus_assoc. rewrite (plus_comm m n). rewrite <- (plus_assoc n m).
    reflexivity.
Qed.

Lemma mult_comm : forall n m : nat, n * m = m * n.
  intros n m. induction n; simpl.
  - symmetry. apply mult_n_0.
  - rewrite IHn. rewrite mult_n_Sm. reflexivity.
Qed.

Lemma mult_plus_distr_r : forall n m p : nat, (n + m) * p = n * p + m * p.
  intros n m p. repeat (rewrite (mult_comm _ p)). apply mult_plus_distr_l.
Qed.

Lemma mult_assoc : forall n m p : nat, n * (m * p) = n * m * p.
  intros n m p. induction n.
  - simpl. reflexivity.
  - simpl. rewrite IHn. symmetry. apply mult_plus_distr_r.
Qed.

Theorem ex_3_2_3 : forall (x n m : nat), power x (n + m) = power x n * power x m.
  intros x n m. induction n.
  - simpl. rewrite plus_n_0. reflexivity.
  - simpl. rewrite IHn. rewrite mult_assoc. reflexivity.
Qed.

Theorem ex_3_2_4 : forall (n m p : nat), n + (m + p) = n + m + p.
  exact plus_assoc.
Qed.

Theorem ex_3_2_5 : forall (k m n : nat), k * (m + n) = k * m + k * n.
  exact mult_plus_distr_l.
Qed.

Section ex_3_2_6.
  Fixpoint op (n m : nat) : nat :=
    match (n, m) with
      | (O, _) => O
      | (S _, O) => O
      | (S p, S q) => S (op p q)
    end.

  Theorem op_is_min : forall (n m : nat), op n m = min n m.
    intros n m. induction n; simpl; reflexivity.
  Qed.
End ex_3_2_6.
