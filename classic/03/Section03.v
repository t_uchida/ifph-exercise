Require Import Program Arith.

Fixpoint foldn {A} (f : A -> A) (x : A) (n : nat) : A :=
  match n with
    | O => x
    | S p => f (foldn f x p)
  end.

Theorem foldn_fusion_law : forall A (f g h : A -> A) (a b : A),
    f a = b -> compose f g = compose h f -> compose f (foldn g a) = foldn h b.
  intros A f g h a b H0 H1. apply functional_extensionality. intro x. unfold compose. induction x.
  - unfold foldn. apply H0.
  - simpl. rewrite <- IHx. generalize (foldn g a x). intro x'.
    fold (compose f g x'). fold (compose h f x'). rewrite H1. reflexivity.
Qed.

Theorem ex_3_3_1 : forall n, foldn S O n = n.
  intro n. induction n.
  - reflexivity.
  - simpl. f_equal. apply IHn.
Qed.

Lemma foldn_plus : forall m n, foldn S m n = m + n.
  intros m n. induction n; simpl.
  - rewrite plus_0_r. reflexivity.
  - rewrite <- plus_n_Sm. rewrite IHn. reflexivity.
Qed.

Lemma foldn_fusion_law_for_plus : forall n, compose (plus n) (foldn S O) = foldn S n.
  intro n. apply foldn_fusion_law.
  - rewrite plus_0_r. reflexivity.
  - apply functional_extensionality. intro x. unfold compose. symmetry. apply plus_n_Sm.
Qed.

Theorem ex_3_3_2 : forall m n,
    (compose (plus m) (foldn S O)) n = (compose (plus n) (foldn S O)) m.
  intros m n. repeat (rewrite foldn_fusion_law_for_plus). repeat (rewrite foldn_plus).
  apply plus_comm.
Qed.

Lemma lt_acc : forall n, Acc lt n.
  intro n. split. induction n.
  - split. intros y0 H0. contradict H. apply lt_n_0.
  - intros y H. case (le_lt_eq_dec _ _ (lt_n_Sm_le _ _ H)).
    + apply IHn.
    + intros H0. rewrite H0. split. apply IHn.
Qed.

(*
Definition div (m n : nat) (H : 0 < n) : nat.
  refine (Fix lt_acc (fun _ => nat)
    (fun (x : nat) (f : forall y : nat, lt y x -> nat) =>
      match lt_dec x n with
        | left _ => O
        | right _ => S (f (x - n) _)
      end) m).
  abstract (apply (lt_minus _ _ (not_lt _ _ n0) H)).
Defined.

Theorem ex_3_3_3 : forall m n (H : 0 < n), div (m * n) n H = m.
  intros m n H. apply (well_founded_ind lt_acc (fun m' => div (m' * n) n H = m')).
  intros x H'. induction x.
    simpl. apply (well_founded_ind lt_acc (fun _ => div (0 * n) n H = 0)).
      intros x H''. apply (H'' 0). admit.
    apply m.
    simpl. admit.
Qed.

Fixpoint div' (x m n : nat) : nat :=
  match x with
    | O => O
    | S y =>
      match lt_dec m n with
        | left _ => O
        | right _ => S (div' y (m - n) n)
      end
  end.

Definition div (m n : nat) : nat := div' m m n.

Theorem ex_3_3_3 : forall m n, div (m * n) n = m.
  intros m n. unfold div. induction m; simpl.
    reflexivity.
*)

Require Import Div2.

Definition log (n : nat) : nat.
  refine (Fix lt_acc (fun _ => nat)
    (fun (x : nat) (f : forall y : nat, lt y x -> nat) =>
      match O_or_S x with
        | inleft (exist p _) => S (f (div2 x) _)
        | inright _ => O
      end) n).
  abstract (apply lt_div2; rewrite <- e; apply lt_0_Sn).
Defined.

Fixpoint power n p :=
  match p with
    | 0 => 1
    | S p' => n * power n p'
  end.

Theorem ex_3_3_4 : forall n, power 2 (log n) = n.
  admit.
Qed.