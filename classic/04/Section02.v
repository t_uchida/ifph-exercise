Require Import List Arith Program NPeano.

Theorem ex_4_2_3 : forall {A : Type} (xs : list A), xs ++ nil = xs.
  intuition.
Qed.

Fixpoint concat {A : Type} (ls : list (list A)) :=
  match ls with
    | nil => nil
    | (x :: ls') => x ++ concat ls'
  end.

Theorem ex_4_2_4 : forall {A : Type} (xss yss : list (list A)), concat (xss ++ yss) = concat xss ++ concat yss.
  intros. induction xss.
    auto.
    simpl. rewrite IHxss. intuition.
Qed.

Theorem ex_4_2_5 : forall {A : Type} (xs ys : list A), length (xs ++ ys) = length xs + length ys.
  intros. induction xs.
    auto.
    simpl. rewrite IHxs. reflexivity.
Qed.

Fixpoint replicate {A : Type} (n : nat) (ls : list A) :=
  match n with
    | O => nil
    | S n' => ls :: replicate n' ls
  end.

Definition list_times {A : Type} (n : nat) (ls : list A) := concat (replicate n ls).

Lemma replicate_concat : forall {A : Type} (ls : list A) (n m : nat), replicate n ls ++ replicate m ls = replicate (n + m) ls.
  intros. induction n.
    auto.
    simpl. rewrite IHn. reflexivity.
Qed.

Theorem ex_4_2_9 : forall (A : Type) (xs ys ls : list A) (n m : nat), xs = list_times n ls -> ys = list_times m ls -> xs ++ ys = ys ++ xs.
  intros. subst. unfold list_times. repeat (rewrite <- ex_4_2_4). f_equal. induction n.
    simpl. intuition.
    repeat (rewrite replicate_concat). f_equal. intuition.
Qed.

Fixpoint index {A : Type} (n : nat) (xs : list A) :=
  match xs, n with
    | nil, _ => None
    | x :: _, O => Some x
    | _ :: xs', S n' => index n' xs'
  end.

Fixpoint drop {A : Type} (n : nat) (xs : list A) :=
  match xs, n with
    | nil, _ => nil
    | xs, O => xs
    | _ :: xs', S n' => drop n' xs'
  end.

Lemma index_n_nil : forall (A : Type) (n : nat), index n nil = (None : option A).
  destruct n; auto.
Qed.

Lemma index_Sn_xs : forall (A : Type) (n : nat) (x : A) (xs : list A), index (S n) (x :: xs) = index n xs.
  destruct xs, n; auto.
Qed.

Lemma drop_O_xs : forall (A : Type) (xs : list A), drop 0 xs = xs.
  destruct xs; auto.
Qed.

Lemma drop_n_nil : forall (A : Type) (n : nat), drop n nil = (nil : list A).
  destruct n; auto.
Qed.

Lemma drop_Sn_xs : forall (A : Type) (n : nat) (x : A) (xs : list A), drop (S n) (x :: xs) = drop n xs.
  destruct xs, n; auto.
Qed.

Create HintDb chapter04.

Hint Rewrite index_n_nil index_Sn_xs drop_O_xs drop_n_nil drop_Sn_xs : chapter04.

Theorem ex_4_2_11 : forall (A : Type) (n m : nat) (xs : list A), index n (drop m xs) = index (m + n) xs.
  intros. destruct xs.
    autorewrite with chapter04 using try reflexivity.
    generalize n m a xs. induction m0.
      intros. autorewrite with chapter04 using try reflexivity.
      induction xs0; simpl.
        autorewrite with chapter04 using try reflexivity.
        auto.
Qed.

Require Import Coq.Arith.Bool_nat.
Require Import Omega.

Lemma if_lt_ge_dec_Sn_Sm : forall (A : Type) (n m : nat) (x y : A),
    (if lt_ge_dec (S n) (S m) then x else y) = (if lt_ge_dec n m then x else y).
  intros. case (lt_ge_dec n m), (lt_ge_dec (S n) (S m)); try auto; omega.
Qed.

Theorem ex_4_2_12 : forall (A : Type) (k : nat) (xs ys : list A),
    index k (xs ++ ys) =
      if lt_ge_dec k (length xs) then index k xs else index (k - length xs) ys.
  intros A k xs ys. case xs; induction k; simpl; auto.
    intros a l. case l.
      simpl. rewrite <- minus_n_O. auto.
      intros a0 l0. rewrite if_lt_ge_dec_Sn_Sm. apply IHk.
Qed.
