Require Import List Arith Program NPeano.

Fixpoint concat {A : Type} (ls : list (list A)) :=
  match ls with
    | nil => nil
    | (x :: ls') => x ++ concat ls'
  end.

Theorem ex_4_3_4a : forall (A B : Type) (f : A -> B) (xs ys : list A), map f (xs ++ ys) = map f xs ++ map f ys.
  intros. induction xs; simpl; congruence.
Qed.

Theorem ex_4_3_4b : forall (A B : Type) (f : A -> B) (xss : list (list A)), map f (concat xss) = concat (map (map f) xss).
  intros. induction xss.
    auto.
    simpl. rewrite <- IHxss. rewrite <- ex_4_3_4a. reflexivity.
Qed.

Definition box {A : Type} (p : A -> bool) := fun x => if p x then (x :: nil) else nil.
Definition filter' {A : Type} (p : A -> bool) (ls : list A) := concat (map (box p) ls).

Theorem ex_4_3_6 : forall (A : Type) (p : A -> bool) (ls : list A), filter' p ls = filter p ls.
  intros. induction ls.
    auto.
    simpl. unfold filter', map, box. induction (p a); auto.
      simpl. f_equal. auto.
Qed.
