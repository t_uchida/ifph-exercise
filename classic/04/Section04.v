Require Import List Arith Program NPeano.

Definition cross {A B C D : Type} (f : (A -> C) * (B -> D)) (x : A * B) := ((fst f) (fst x), (snd f) (snd x)).
Definition pair {A B C : Type} (f : (A -> B) * (A -> C)) (x : A) := ((fst f) x, (snd f) x).
Definition unzip {A B : Type} : list (A * B) -> list A * list B := pair (map fst, map snd).

Hint Unfold compose map cross pair unzip.

Theorem ex_4_4_3a : forall (A B C : Type) (f : B -> C) (g : A -> B), map f \u2218 map g = map (f \u2218 g).
  intros. apply functional_extensionality. intro xs. induction xs; auto. autounfold. f_equal. auto.
Qed.

Theorem ex_4_4_3b : forall (A B C D E : Type) (f : (A -> B) * (A -> C)) (g : (B -> D) * (C -> E)),
    cross g \u2218 pair f = pair (fst g \u2218 fst f, snd g \u2218 snd f).
  intros. autounfold. auto.
Qed.

Theorem ex_4_4_3c : forall (A B C D : Type) (f : A -> B) (g : (B -> C) * (B -> D)), pair g \u2218 f = pair (fst g \u2218 f, snd g \u2218 f).
  intros. autounfold. auto.
Qed.

Theorem ex_4_4_3d : forall (A B C D : Type) (f : (A -> C) * (B -> D)), fst \u2218 (cross f) = fst f \u2218 fst.
  intros. autounfold. auto.
Qed.

Theorem ex_4_4_3e : forall (A B C D : Type) (f : (A -> C) * (B -> D)), snd \u2218 (cross f) = snd f \u2218 snd.
  intros. autounfold. auto.
Qed.

Hint Rewrite ex_4_4_3a ex_4_4_3b ex_4_4_3c ex_4_4_3d ex_4_4_3e : chapter04.

Theorem ex_4_4_3 : forall (A B C D : Type) (f : (A -> C)) (g : (B -> D)), cross (map f, map g) \u2218 unzip = unzip \u2218 map (cross (f, g)).
  intros. unfold unzip. autorewrite with chapter04 using try simpl. reflexivity.
Qed.
