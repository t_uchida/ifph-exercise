Require Import List Arith Program NPeano.

Fixpoint inits {A : Type} (xs : list A) :=
  match xs with
    | nil => nil :: nil
    | x :: xs' => nil :: map (fun z => x :: z) (inits xs')
  end.

Definition foldl {A B : Type} (f : A -> B -> A) (e : A) (xs : list B) := fold_left f xs e.
Definition scanl {A B : Type} (f : A -> B -> A) (e : A) := compose (map (fun z => fold_left f z e)) inits.

Theorem ex_4_5_7 : forall (A B : Type) (f : A -> B -> A) (e : A), scanl f e nil = e :: nil.
  auto.
Qed.

Inductive Liste (A : Set) : Set :=
  | Nil : Liste A
  | Snoc : Liste A -> A -> Liste A.

Fixpoint convert' {A : Set} (xs : Liste A) (ys : list A) :=
  match xs with
    | Nil => ys
    | Snoc xs' x => convert' xs' (x :: ys)
  end.

Definition convert {A : Set} (xs : Liste A) := convert' xs nil.

Fixpoint folde {A B: Set} (f : B -> A -> B) (e : B) (xs : Liste A) :=
  match xs with
    | Nil => e
    | Snoc xs' x => f (folde f e xs') x
  end.

Lemma convert_snoc : forall {A : Set} (xs : Liste A) (ys : list A) (a : A),
    convert' xs (ys ++ [a]) = convert' xs ys ++ [a].
  intros A xs. induction xs; simpl.
    reflexivity.
    intros ys a'. rewrite <- IHxs. auto.
Qed.

Theorem ex_4_5_11 : forall {A B : Set} (f : B -> A -> B) (e : B),
    folde f e = compose (foldl f e) convert.
  intros. apply functional_extensionality. intro x. unfold compose. induction x; simpl.
    reflexivity.
    rewrite IHx. unfold convert. simpl. unfold foldl. rewrite <- (app_nil_l [a]).
    rewrite convert_snoc. generalize (convert' x []), e. intro l. induction l; simpl.
      reflexivity.
      intro e'. rewrite <- IHl. reflexivity.
Qed.
