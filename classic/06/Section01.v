Require Import List Arith Omega Program Sorted.

Set Implicit Arguments.

Inductive Btree (A : Set) : Set :=
  | Leaf : A -> Btree A
  | Fork : Btree A -> Btree A -> Btree A.

Fixpoint size (A : Set) (t : Btree A) :=
  match t with
    | Leaf _ => 1
    | Fork t1 t2 => size t1 + size t2
  end.

Fixpoint nodes {A : Set} (t : Btree A) :=
  match t with
    | Leaf _ => 0
    | Fork t1 t2 => 1 + nodes t1 + nodes t2
  end.

Theorem ex_6_1_2 : forall (A : Set) (t : Btree A), size t = 1 + nodes t.
  intros. elim t.
    auto.
    intros. simpl. intuition.
Qed.

Fixpoint subtrees {A : Set} (t : Btree A) :=
  match t with
    | Leaf x => Leaf x :: nil
    | Fork t1 t2 => Fork t1 t2 :: subtrees t1 ++ subtrees t2
  end.

Theorem ex_6_1_3 : forall (A : Set) (t : Btree A), length (subtrees t) = size t + nodes t.
  intros. elim t.
    auto.
    intros. simpl. rewrite app_length. intuition.
Qed.

Fixpoint down {A : Set} (n : nat) (t : Btree A) :=
  match t with
    | Leaf _ => Leaf n
    | Fork t1 t2 => Fork (down (S n) t1) (down (S n) t2)
  end.

Definition depths {A : Set} : Btree A -> Btree nat := down O.

Fixpoint maxBtree (t : Btree nat) :=
  match t with
    | Leaf x => x
    | Fork t1 t2 => max (maxBtree t1) (maxBtree t2)
  end.

Definition height {A : Set} (t : Btree A) := maxBtree (depths t).

Lemma max_plus_n : forall x y n, max (n + x) (n + y) = n + max x y.
  intros x y. induction n; simpl; intuition.
Qed.

Lemma max_le_plus : forall n m, max n m <= n + m.
  intros. elim (dec_le n m); intros; [ assert (max n m = m) | assert (max n m = n) ]; intuition.
Qed.

Lemma maxBtree_down : forall (A : Set) (t : Btree A) (n : nat), maxBtree (down n t) = n + height t.
  intros A t. elim t.
    auto.
    intros. simpl. unfold height. rewrite H, H0. simpl. rewrite H, H0, max_plus_n. simpl. auto.
Qed.

Lemma le_1_size : forall A (t : Btree A), 1 <= size t.
  intros. elim t.
    auto.
    intros. simpl. intuition.
Qed.

Theorem ex_6_1_4 : forall (A : Set) (t : Btree A), height t < size t.
  intros. elim t.
    auto.
    intros. unfold height. simpl. repeat (rewrite maxBtree_down). rewrite max_plus_n.
    destruct (Max.max_dec (height b) (height b0)).
      rewrite e. rewrite (plus_comm (size b) _). apply (plus_le_lt_compat _ _ _ _ (le_1_size _) H).
      rewrite e. apply (plus_le_lt_compat _ _ _ _ (le_1_size _) H0).
Qed.

Theorem ex_6_1_6' : forall (A : Set) (n : nat) (t : Btree A), n + height t = maxBtree (down n t).
  intros. generalize n. elim t.
    auto.
    intros. simpl. rewrite <- H. rewrite <- H0. simpl.
    cut (forall A (t1 t2 : Btree A), height (Fork t1 t2) = S (max (height t1) (height t2))).
      intro H1. rewrite H1. rewrite max_plus_n. rewrite plus_n_Sm. auto.
    intros. unfold height. unfold depths. simpl. repeat (rewrite maxBtree_down). simpl. auto.
Qed.

Theorem ex_6_1_6 : forall (A : Set) (t : Btree A), height t = maxBtree (depths t).
  intros. unfold depths. rewrite <- plus_O_n at 1. apply ex_6_1_6'.
Qed.

