Require Import List Arith Omega Program Sorted.

Set Implicit Arguments.

Class Ord (A : Set) := cmp : A -> A -> comparison.

Instance OrdNat : Ord nat := { cmp := nat_compare }.

Definition cmp_dec {A} `{Ord A} (x y : A) : {cmp x y = Lt} + {cmp x y = Eq} + {cmp x y = Gt}.
  destruct (cmp x y); auto.
Defined.

Definition le {A} `{Ord A} (x y : A) :=
  match cmp x y with
    | Lt => true
    | Eq => true
    | Gt => false
  end.

Definition Le {A} `{Ord A} (x y : A) := Bool.Is_true (le x y).

Inductive Stree (A : Set) : Set :=
  | SNull : Stree A
  | SFork : Stree A -> A -> Stree A -> Stree A.

Fixpoint flatten {A : Set} (t : Stree A) :=
  match t with
    | SNull => nil
    | SFork t1 y t2 => flatten t1 ++ y :: flatten t2
  end.

Fixpoint member {A} `{Ord A} (x : A) (t : Stree A) :=
  match t with
    | SNull => false
    | SFork t1 y t2 => match cmp x y with
      | Lt => member x t1
      | Eq => true
      | Gt => member x t2
    end
  end.

Fixpoint height' {A : Set} (t : Stree A) :=
  match t with
    | SNull => 0
    | SFork t1 y t2 => S (max (height' t1) (height' t2))
  end.

Fixpoint size' {A : Set} (t : Stree A) :=
  match t with
    | SNull => 0
    | SFork t1 y t2 => 1 + size' t1 + size' t2
  end.

Definition power n m := nat_iter m (mult n) 1.

Lemma power_2_Sn : forall n, power 2 (S n) = 2 * power 2 n.
  auto.
Qed.

Lemma lt_0_power_2_n : forall n, 0 < power 2 n.
  intro n. elim n.
    auto.
    intros p H. rewrite power_2_Sn. rewrite (mult_n_O 2) at 1. auto with arith.
Qed.

Lemma power_le_n_m : forall n m, n <= m -> power 2 n <= power 2 m.
  intros n m H. elim H; auto.
    intros p H0 H1. rewrite power_2_Sn. omega.
Qed.

Theorem ex_6_2_1 : forall (A : Set) (t : Stree A), height' t <= size' t < power 2 (height' t).
  split; elim t; try auto; intros; simpl.
    apply le_n_S. apply Max.max_lub; omega.
    rewrite power_2_Sn. apply (lt_le_trans _ (power 2 (height' s) + power 2 (height' s0))).
      omega.
      assert (forall n, 2 * n = n + n). intro. omega.
      rewrite H1. apply (le_trans _ (power 2 (height' s) + power 2 (max (height' s) (height' s0)))).
        assert (height' s0 <= max (height' s) (height' s0)). apply Max.le_max_r.
        apply (plus_le_compat_l _ _ _ (power_le_n_m H2)).
        assert (height' s <= max (height' s) (height' s0)). apply Max.le_max_l.
        apply (plus_le_compat_r _ _ _ (power_le_n_m H2)).
Qed.

Fixpoint insert {A} `{Ord A} (x : A) (t : Stree A) :=
  match t with
    | SNull => SFork (SNull A) x (SNull A)
    | SFork t1 y t2 =>
        match cmp x y with
          | Lt => SFork (insert x t1) y t2
          | Eq => SFork t1 y t2
          | Gt => SFork t1 y (insert x t2)
        end
  end.

Fixpoint join {A : Set} (t1 t2 : Stree A) :=
  match t1 with
    | SNull => t2
    | SFork t3 x t4 => SFork t3 x (join t4 t2)
  end.

Fixpoint delete {A} `{Ord A} (x : A) (t : Stree A) :=
  match t with
    | SNull => SNull A
    | SFork t1 y t2 =>
        match cmp x y with
          | Lt => SFork (delete x t1) y t2
          | Eq => join t1 t2
          | Gt => SFork t1 y (delete x t2)
        end
  end.

Definition headTree {A} `{Ord A} (t : Stree A) := head (flatten t).

(* Definition splitTree {A : Set} {X : Ord A} (t : Stree A) := (headTree t, tailTree t). *)
Definition splitTree {A} `{Ord A} (t : Stree A) : option A * Stree A.
  admit.
Qed.

Definition empty {A : Set} (t : Stree A) :=
  match t with
  | SNull => true
  | SFork _ _ _ => false
  end.

Fixpoint splitTree' {A} `{Ord A} (t : Stree A) :=
  match t with
  | SNull => (None, SNull A)
  | SFork xt y yt =>
      match empty xt with
      | true => (Some y, yt)
      | false => let (x, wt) := splitTree' xt in (x, SFork wt y yt)
      end
  end.
(*
Definition inordered {A : Set} {X : Ord A} (t : Stree A) := LocallySorted Le (flatten t).

Lemma ord_dec : forall (A : Set) (X : Ord A) (x y : A), {cmp x y = Eq} + {cmp x y = Lt} + {cmp x y = Gt}.
  intros. case (cmp x y); auto.
Qed.

Lemma locally_sorted_cons : forall {A : Set} {X : Ord A} (l : list A) (a : A),
    LocallySorted Le (a :: l) -> LocallySorted Le l.
  intros. induction l.
    apply LSorted_nil.
    admit.
Qed.

Lemma locally_sorted_app : forall {A : Set} {X : Ord A} (l l' : list A),
    LocallySorted Le (l ++ l') -> LocallySorted Le l /\ LocallySorted Le l'.
  intros. split.
    induction l. apply LSorted_nil.
    admit.
    induction l.
      auto with arith.
      rewrite <- app_comm_cons in H. apply IHl. apply (locally_sorted_cons H).
Qed.

Lemma inordered_sfork : forall {A : Set} {X : Ord A} (t1 t2 : Stree A) (a : A),
    inordered (SFork t1 a t2) -> inordered t1 /\ inordered t2.
  unfold inordered. simpl.
  intros. split.
    apply (proj1 (locally_sorted_app _ _ H)).
    apply (locally_sorted_cons (proj2 (locally_sorted_app _ _ H))).
Qed.

Lemma cons_app : forall {A : Set} (l l' : list A) (a : A), l ++ a :: l' = (l ++ [a]) ++ l'.
  intros. rewrite <- app_assoc. auto.
Qed.

Lemma sorted_app_cons_1 : forall {A : Set} {X : Ord A} (l l' : list A) (a : A),
    LocallySorted Le (l ++ a :: l') -> LocallySorted Le (l ++ [a]) /\ LocallySorted Le (a :: l').
  intros. split.
    rewrite cons_app in H. apply (proj1 (locally_sorted_app _ _ H)).
    apply (proj2 (locally_sorted_app _ _ H)).
Qed.

Theorem ex_6_2_4 : forall (A : Set) (X : Ord A) (t : Stree A) (x : A), inordered t -> inordered (insert x t).
  intros. unfold inordered. induction t.
    simpl. apply LSorted_cons1.
    intros. simpl. induction (ord_dec X x a).
      elim a0; intro H2; rewrite H2.
        unfold inordered in H. apply H.
        simpl. unfold inordered in H. simpl in H. admit.
      rewrite b. simpl. unfold inordered in H. simpl in H. admit.
Qed.
*)

Definition inordered {A} `{Ord A} (t : Stree A) := Sorted Le (flatten t).

Lemma ord_dec : forall A `(Ord A) (x y : A), {cmp x y = Eq} + {cmp x y = Lt} + {cmp x y = Gt}.
  intros. case (cmp x y); auto.
Qed.

Theorem ex_6_2_4 : forall A `(Ord A) (t : Stree A) (x : A), inordered t -> inordered (insert x t).
  unfold inordered. intros. induction t.
    simpl. apply (Sorted_cons (Sorted_nil Le) (HdRel_nil Le _)).
    intros. simpl. elim (ord_dec H x a).
      intro a0. elim a0; intro H2; rewrite H2; auto.
        simpl. admit.
      intro a0. rewrite a0. simpl. admit.
Qed.

Theorem ex_6_2_5 : forall (A : Set) (xt yt : Stree A) (hx hy : nat),
    hx = height' xt -> hy = height' yt -> max hx hy <= height' (join xt yt) <= max hx hy + 1.
  intros. split.
    induction xt.
      simpl. rewrite <- H0. simpl in H. rewrite H. auto.
      simpl. simpl in H. rewrite H, H0. repeat (rewrite Max.succ_max_distr).
      rewrite <- Max.max_assoc. admit.
    induction xt.
      simpl. simpl in H. rewrite H. simpl. omega.
      simpl. simpl in H. admit.
Qed.

Theorem ex_6_2_6 : forall A `(Ord A) (t : Stree A), splitTree t = splitTree' t.
  admit.
Qed.
