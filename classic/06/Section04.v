Require Import List Arith Omega Program Sorted.

Set Implicit Arguments.

Inductive Btree (A : Set) : Set :=
  | Leaf : A -> Btree A
  | Fork : Btree A -> Btree A -> Btree A.

Class Ord (A : Set) := { cmp : A -> A -> comparison }.

Instance OrdNat : Ord nat := { cmp := nat_compare }.

Inductive Rose (A : Set) : Set := Node : A -> list (Rose A) -> Rose A.

Fixpoint size'' {A : Set} (t : Rose A) :=
  match t with
    (Node a l) => 1 + fold_left plus (map size'' l) 0
  end.

Fixpoint height'' {A : Set} (t : Rose A) :=
  match t with
    (Node a l) => 1 + fold_left max (map height'' l) 0
  end.

Fixpoint down'' {A : Set} (n : nat) (t : Rose A) :=
  match t
    with (Node a l) => Node n (map (down'' (S n)) l)
  end.

Definition depths'' {A : Set} (t : Rose A) := down'' 1 t.

Definition gmax {A : Set} {X : Ord A} (x y : A) :=
  match cmp x y with
    | Lt => y
    | _ => x
  end.

Fixpoint maxRose {A : Set} {X : Ord A} (t : Rose A) :=
  match t with
    (Node x l) => fold_left gmax (map maxRose l) x
  end.

Theorem ex_6_4_1 : forall (A : Set) (X : Ord A) (t : Rose A), height'' t = maxRose (depths'' t).
  admit.
Qed.

Fixpoint toB {A : Set} (t : Rose A) :=
  match t with (Node x l) => fold_left (fun (t t' : Btree A) => Fork t t') (map toB l) (Leaf x)
end.

