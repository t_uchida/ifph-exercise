Require Import List Program Arith.
Set Implicit Arguments.

Class Queue (Q : nat -> Type -> Type) := {
  empty : forall {A}, Q 0 A;
  join : forall {A n}, A -> Q n A -> Q (S n) A;
  front_def : forall {A n}, Q (S n) A -> A;
  back_def : forall {A n}, Q (S n) A -> Q n A;
  queue_1st_law : forall {A} (x : A), front_def (join x empty) = x;
  queue_2nd_law : forall {A n} (q : Q n A) (x y : A), front_def (join x (join y q)) = front_def (join y q);
  queue_3rd_law : forall {A} (x : A), back_def (join x empty) = empty;
  queue_4th_law : forall {A n} (q : Q n A) (x y : A), back_def (join x (join y q)) = join x (back_def (join y q))
}.

Definition check_empty {Q n A} `{Queue Q} (q : Q n A) : {n = 0} + {0 < n} := zerop n.

Theorem queue_5th_law : forall {Q A} `{Queue Q}, check_empty (empty : Q 0 A) = left eq_refl.
  auto.
Qed.

Theorem queue_6th_law : forall {Q n A} `{Queue Q} (q : Q (S n) A), check_empty q = right (lt_0_Sn n).
  admit.
Qed.

Definition to_Q_Sm_A {Q n m A} `{Queue Q} (H' : S m = n) (q : Q n A) : Q (S m) A.
  rewrite H'. exact q.
Defined.

Definition front {Q n m A} `{Queue Q} {H'' : S m = n} (H' : 0 < n) (q : Q n A) : A :=
  front_def (to_Q_Sm_A H'' q).

Definition front' {Q n A} `{Queue Q} (q : Q n A) : option A :=
  match O_or_S n with
    | inleft (exist _ H') => Some (front_def (to_Q_Sm_A H' q))
    | inright _ => None
  end.

Definition back {Q n m A} `{Queue Q} {H'' : S m = n} (H' : 0 < n) (q : Q n A) : Q m A :=
  back_def (to_Q_Sm_A H'' q).

Inductive ListQueue (n : nat) (A : Type) : Type := list_queue_def : list A -> ListQueue n A.

Fixpoint last' {A} (l : list A) (H : [] <> l) {struct l} : A.
  refine (match destruct_list l with
    | inleft (existT x (exist xs _)) => match destruct_list xs with
        | inleft (existT _ (exist _ H)) => last' A xs _
        | inright _ => x
      end
    | inright _ => !
  end).
  destruct s1. destruct s1. rewrite e0. apply nil_cons.
  unfold not in H. apply H. symmetry. apply e.
Abort.

Axiom last' : forall {A} (l : list A), [] <> l -> A.
Definition without_last A (l : list A) (H : [] <> l) : list A := [].

Instance ListQueue_Queue : Queue ListQueue := {
  empty A := list_queue_def 0 [];
  join A n x q := match q with list_queue_def xs => list_queue_def (S n) (x :: xs) end;
  front_def A n q := match q with list_queue_def xs => last' _ xs end;
  back_def A n q :=
    match q with list_queue_def xs => list_queue_def n (without_last _ xs) end
}.
