Require Import Ssreflect.ssreflect Ssreflect.ssrfun Ssreflect.ssrbool Ssreflect.eqtype Ssreflect.ssrnat.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Variable A : Type.
Implicit Type x a b : A.
Implicit Type n m p k : nat.
Implicit Type op : A -> A -> A.

Theorem ex_3_2_1 n : 1 * n = n.
  elim: n => // n P. by rewrite -{2}P.
Qed.

Theorem ex_3_2_2 op a b : (forall x, op a x = x) -> (forall x, op x b = x) -> a = b.
  move=> P Q. by rewrite -(P b) (Q a).
Qed.

Fixpoint power m n := if n is n'.+1 then m * power m n' else 1.

Theorem ex_3_2_3 m n p : power p (m + n) = power p m * power p n.
  elim: m => [|m P] //.
    - by rewrite add0n mul1n.
    - by rewrite addSn {1 2}/power -/power P mulnA.
Qed.

Theorem ex_3_2_4 m n p : (m + n) + p = m + (n + p).
  elim: m => [|m P] //. by rewrite 3!addSn P.
Qed.

Theorem ex_3_2_5 m n k : k * (m + n) = k * m + k * n.
  elim: k => [|k P] //. rewrite 3!mulSn P -2!ex_3_2_4. f_equal. rewrite 2!ex_3_2_4. f_equal. apply addnC.
Qed.

Fixpoint opn m n :=
  match (m, n) with
    | (0, _) => 0
    | (_.+1, 0) => 0
    | (m.+1, n.+1) => (opn m n).+1
  end.

Theorem ex_3_2_6 m n : opn m n = min m n.
  elim: m => //.
Qed.

