Require Import ssreflect seq ssrnat.

Variable T : Type.
Implicit Type x : T.
Implicit Type s t u : seq T.
Implicit Type ss tt : seq (seq T).
Implicit Type n m : nat.

Theorem ex_4_2_3 s : s ++ [::] = s.
  by elim: s => //= x s ->.
Qed.

Fixpoint concat ss := if ss is s :: ss' then s ++ concat ss' else [::].

Theorem ex_4_2_4 ss tt : concat (ss ++ tt) = concat ss ++ concat tt.
  elim: ss => //= x s ->. apply catA.
Qed.

Theorem ex_4_2_5 s t : length (s ++ t) = length s + length t.
  by elim: s => //= x s ->.
Qed.

Fixpoint replicate n s := if n is n'.+1 then s :: replicate n' s else nil.

Definition list_times n s := concat (replicate n s).

Lemma replicate_concat s n m : replicate n s ++ replicate m s = replicate (n + m) s.
  by elim: n => //= n ->.
Qed.

Theorem ex_4_2_9 s t u n m : s = list_times n u -> t = list_times m u -> s ++ t = t ++ s.
  rewrite /list_times => -> ->. do 2 rewrite -ex_4_2_4 replicate_concat. by rewrite addnC.
Qed.

Fixpoint index n s :=
  match n, s with
    | _, nil => None
    | 0, x :: _ => Some x
    | n'.+1, _ :: s' => index n' s'
  end.

Lemma index_n_nil n : index n nil = None. by elim: n. Qed.

Lemma index_Sn_xs n x s : index n.+1 (x :: s) = index n s.
  by elim: s n.
Qed.

Fixpoint drop n s :=
  match n, s with
    | _, nil => nil
    | 0, _ => s
    | n'.+1, x :: s' => drop n' s'
  end.

Lemma drop_0_s s : drop 0 s = s.
  by elim: s.
Qed.

Lemma drop_n_nil n : drop n nil = nil.
  by elim: n.
Qed.

Lemma drop_Sn_xs n x s : drop n.+1 (x :: s) = drop n s.
  by elim: s n.
Qed.

Theorem ex_4_2_11 n m s : index n (drop m s) = index (m + n) s.
  move: m. elim: s.
    - move=> m. rewrite drop_n_nil. by do 2 rewrite index_n_nil.
    - move=> a l IHs m. elim: m.
      * by rewrite drop_0_s.
      * move=> n' IHm. rewrite addSn index_Sn_xs drop_Sn_xs. apply IHs.
Qed.

