Require Import ssreflect seq ssrnat.

Variable A B : Type.
Implicit Type f : A -> B.
Implicit Type p : A -> bool.

Fixpoint concat {T} (ss : seq (seq T)) := if ss is s :: ss' then s ++ concat ss' else nil.

Theorem ex_4_3_4a f s t : map f (s ++ t) = map f s ++ map f t.
  by elim: s => //= a s ->.
Qed.

Theorem ex_4_3_4b f ss : map f (concat ss) = concat (map (map f) ss).
  elim: ss => //= a ss <-. apply ex_4_3_4a.
Qed.

Definition box p a := if p a then [:: a ] else nil.
Definition filter' p s := concat (map (box p) s).

Theorem ex_4_3_6 p s : filter' p s = filter p s.
  elim: s => //= a ss <-. rewrite {1}/filter' => /=. rewrite {1}/box. by elim: (p a).
Qed.

