Require Import ssreflect seq ssrfun.

Definition cross {A B C D} (f : (A -> C) * (B -> D)) (x : A * B) := ((fst f) (fst x), (snd f) (snd x)).
Definition pair {A B C} (f : (A -> B) * (A -> C)) (x : A) := ((fst f) x, (snd f) x).
Definition unzip {A B} : seq (A * B) -> seq A * seq B := pair (map (@fst A B), map (@snd A B)).

Theorem ex_4_4_3a A B C (f : B -> C) (g : A -> B) : map f \o map g =1 map (f \o g).
  rewrite /eqfun => x. by elim: x => //= a l ->.
Qed.

Theorem ex_4_4_3b A B C D E (f : (A -> B) * (A -> C)) (g : (B -> D) * (C -> E)) : cross g \o pair f =1 pair (fst g \o fst f, snd g \o snd f).
  by rewrite /cross /pair /comp.
Qed.

Theorem ex_4_4_3c A B C D (f : A -> B) (g : (B -> C) * (B -> D)) : pair g \o f =1 pair (fst g \o f, snd g \o f).
  by rewrite /pair /comp.
Qed.

Theorem ex_4_4_3d A B C D (f : (A -> C) * (B -> D)) : (@fst C D) \o (@cross A B C D f) =1 fst f \o (@fst A B).
  by rewrite /cross /comp /comp {1}/fst.
Qed.

Theorem ex_4_4_3e A B C D (f : (A -> C) * (B -> D)) : (@snd C D) \o (@cross A B C D f) =1 snd f \o (@snd A B).
  by rewrite /cross /comp /comp {1}/snd.
Qed.

Lemma ftrans_ {A B} (f g h : A -> B) : f =1 g -> g =1 h -> f =1 h.
  move => eq_fg eq_gh x. rewrite eq_fg. rewrite eq_gh. reflexivity.
Qed.

Theorem ex_4_4_3 A B C D (f : A -> C) (g : B -> D) : cross (map f, map g) \o unzip =1 unzip \o map (cross (f, g)).
  (* rewrite /unzip => x. rewrite ex_4_4_3b. rewrite ex_4_4_3c /=. rewrite ex_4_4_3a. *)
Abort.

