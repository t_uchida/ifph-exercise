Require Import ssreflect seq ssrfun.

Variable A B : Type.
Implicit Type a : A.
Implicit Type b : B.
Implicit Type s : seq A.
Implicit Type t : seq B.

Fixpoint inits s := if s is x :: s' then nil :: map (fun z => x :: z) (inits s') else nil :: nil.
Definition scanl (f : B -> A -> B) b := map (fun z => foldl f b z) \o inits.

(*
Require Import List Arith Program NPeano.

Fixpoint inits {A : Type} (xs : list A) :=
  match xs with
    | [] => [[]]
    | x :: xs' => [] :: map (fun z => x :: z) (inits xs')
  end.

Definition scanl {A B : Type} (f : A -> B -> A) (e : A) := compose (map (fun z => fold_left f z e)) inits.

Theorem ex_4_6_1 : forall (A B : Type) (f : A -> B -> B) (e : B) (xs : list A), fold_right f e xs = fold_left (flip f) (rev xs) e.
  intros. induction xs; auto.
    simpl. rewrite IHxs. rewrite fold_left_app. unfold fold_left at 2. unfold flip. reflexivity.
Qed.

Theorem ex_4_6_2 : forall (A B C : Type) (f : B -> C) (g : A -> B -> B) (h : A -> C -> C) (a : B),
    (forall (x : A) (y : B), f (g x y) = h x (f y)) -> compose f (fold_right g a) = fold_right h (f a).
  intros. apply functional_extensionality. intro xs. induction xs; auto.
    simpl. rewrite <- IHxs. unfold compose. rewrite <- H with (x := a0) (y := fold_right g a xs). f_equal.
Qed.

Theorem ex_4_6_3 : forall (A B C : Type) (f : B -> C) (g : B -> A -> B) (h : C -> A -> C) (a : B) (xs : list A),
    (forall (x : B) (y : A), f (g x y) = h (f x) y) -> f (fold_left g xs a) = fold_left h xs (f a).
  intros. destruct xs.
    auto.
    simpl. generalize a a0. induction xs; intros; simpl; congruence.
Qed.

Theorem ex_4_6_4a' : forall (A B : Type) (f : B -> A -> B) (a : A) (b : B) (xs ys : list A),
    fold_left f (xs ++ ys) (f b a) = fold_left f ys (fold_left f xs (f b a)).
  intros. destruct xs.
    auto.
    simpl. generalize a b a0. induction xs; intros; simpl; congruence.
Qed.

Theorem ex_4_6_4a : forall (A B : Type) (f : B -> A -> B) (a : B) (xs ys : list A),
    fold_left f (xs ++ ys) a = fold_left f ys (fold_left f xs a).
  Hint Resolve ex_4_6_4a'.
  intros. induction xs; simpl; auto.
Qed.

Theorem ex_4_6_4b : forall (A B : Type) (f : A -> B -> B) (a : B) (xs ys : list A),
    fold_right f a (xs ++ ys) = fold_right f (fold_right f a ys) xs.
  intros. generalize a. induction xs.
    auto.
    intros. simpl. f_equal. apply IHxs.
Qed.

Lemma fold_left_map : forall (A : Type) (f : A -> A -> A) (e x : A) (xs : list (list A)),
    map (fun z : list A => fold_left f z e) (map (fun z : list A => x :: z) xs) = map (fun z : list A => fold_left f z (f e x)) xs.
  intros. induction xs.
    auto.
    simpl. congruence.
Qed.

Lemma scanl_xs : forall (A : Type) (f : A -> A -> A) (e x : A) (xs : list A),
    scanl f e (x :: xs) = e :: scanl f (f e x) xs.
  intros. generalize e. induction xs.
    auto.
    intros. unfold scanl, compose, inits. fold (inits xs). simpl. f_equal. f_equal. apply fold_left_map.
Qed.

Lemma fold_left_f : forall (A : Type) (op : A -> A -> A) (a e : A),
    (forall a, op e a = op a e) ->
    (forall a b c : A, op (op a b) c = op a (op b c)) ->
    (fun z : list A => (op a (fold_left op z e))) = (fun z : list A => fold_left op z (op e a)).
  intros. apply functional_extensionality. intro. rewrite H. generalize e. induction x.
    auto.
    simpl. congruence.
Qed.

Lemma fold_left_map_op : forall (A : Type) (f : A -> A) (op : A -> A -> A) (e : A) (xs : list (list A)),
    map (fun z => f (fold_left op z e)) xs = map f (map (fun z => fold_left op z e) xs).
  intros. induction xs.
    auto.
    simpl. congruence.
Qed.  

Lemma fold_left_scanl : forall (A : Type) (op : A -> A -> A) (e : A) (xs : list A),
    map (fun z : list A => fold_left op z e) (inits xs) = scanl op e xs.
  unfold scanl, compose. reflexivity.
Qed.

Hint Rewrite -> fold_left_f fold_left_map_op fold_left_scanl: chapter04.
Hint Rewrite <- fold_left_f : chapter04.

Section ex_4_6_5.
  Variable A : Type.
  Variable op : A -> A -> A.
  Variable e : A.

  Hypothesis op_unit : forall a, op e a = op a e.
  Hypothesis op_assoc : forall a b c, op (op a b) c = op a (op b c).

  Definition g x xs := e :: map (op x) xs.

  Theorem ex_4_6_5 : scanl op e = fold_right g [e].
    apply functional_extensionality. intro xs. induction xs; auto.
      simpl.
      unfold g at 1. rewrite scanl_xs. f_equal. unfold scanl, compose.
      autorewrite with chapter04; auto. f_equal. assumption.
  Qed.
End ex_4_6_5.

Lemma nil_neq : forall (A : Type), (nil : list A) <> nil -> False.
  intuition.
Qed.

Definition foldr1 {A : Type} (f : A -> A -> A) (xs : list A) : xs <> nil -> A :=
  match xs with
    | nil => fun p : nil <> nil => match nil_neq A p with end
    | x :: xs'=> fun _ => fold_right f x xs'
  end.

Lemma fold_right_map : forall (A : Type) (f : A -> A) (op : A -> A -> A) (a : A) (xs : list A),
    (forall x y, op (f x) (f y) = f (op x y)) ->
    fold_right op (f a) (map f xs) = f (fold_right op a xs).
  intros. induction xs.
    auto.
    simpl. congruence.
Qed.

Section ex_4_6_6.
  Variable A : Type.
  Variable m p : A -> A -> A.

  Hypothesis distr_prop : forall x y z, m x (p y z) = p (m x y) (m x z).

  Lemma map_not_nil : forall x xs, xs <> nil -> map (m x) xs <> nil.
    intros. destruct xs; simpl; congruence.
  Qed.

  Theorem ex_4_6_6 : forall x xs (H0 : xs <> nil), foldr1 p (map (m x) xs) (map_not_nil x xs H0) = m x (foldr1 p xs H0).
    intros. destruct xs.
      congruence.
      simpl. auto using fold_right_map.
  Qed.
End ex_4_6_6.

Definition scanl1 {A : Type} (f : A -> A -> A) (xs : list A) : xs <> nil -> list A :=
  match xs with
    | nil => fun p : nil <> nil => match nil_neq A p with end
    | x :: xs' => fun _ => scanl f x xs'
  end.

Section ex_4_6_8.
  Variable A : Type.
  Variable f g : A -> A -> A.

  Hypothesis distr_prop : forall x y z, g x (f y z) = f (g x y) (g x z).
  Hypothesis assoc_prop : forall x y z, g x (g y z) = g (g x y) z.

  Definition h x y := f x (g x y).

  Lemma scanl1_not_nil : forall (xs : list A) (H : xs <> nil), scanl1 g xs H <> nil.
    intros. induction xs.
      congruence.
      unfold scanl1, scanl, map, compose. destruct xs; simpl; congruence.
  Qed.

  Theorem ex_4_6_8 : forall (xs : list A) (H : xs <> nil), foldr1 f (scanl1 g xs H) (scanl1_not_nil xs H) = foldr1 h xs H.
    intros xs H. induction xs.
      simpl. contradict H. auto.
      simpl. unfold h. unfold fold_right. simpl.
  Abort.
End ex_4_6_8.

Definition foldl1 {A : Type} (f : A -> A -> A) (xs : list A) : xs <> nil -> A :=
  match xs with
    | nil => fun p : nil <> nil => match nil_neq A p with end
    | x :: xs' => fun _ => fold_left f xs' x
  end.

Section ex_4_6_10.
  Variable A : Type.
  Variable m p : A -> A -> A.
  Variable e : A.

  Definition d xy z := let t := m (snd xy) z in (p (fst xy) t, t).

  Lemma scanl_not_nil : forall (xs : list A), scanl m e xs <> [].
    intros. unfold scanl, compose, inits. destruct xs; simpl; congruence.
  Qed.

  Theorem ex_4_6_10 : forall (xs : list A), foldl1 p (scanl m e xs) (scanl_not_nil xs) = fst (fold_left d xs (e, e)).
  Abort.
End ex_4_6_10.
*)
