Require Import ssreflect seq ssrnat ssrbool.

Variable A : Type.
Implicit Type n : nat.

Inductive Btree A :=
  | Leaf : A -> Btree A
  | Fork : Btree A -> Btree A -> Btree A.

Implicit Type t : Btree A.

Fixpoint tsize t := if t is Fork t1 t2 then tsize t1 + tsize t2 else 1.
Fixpoint nodes t := if t is Fork t1 t2 then 1 + nodes t1 + nodes t2 else 0.

Theorem ex_6_1_2 t : tsize t = 1 + nodes t.
  elim: t => // t P1 u P2. by rewrite /tsize -/tsize /nodes -/nodes P1 P2.
Qed.

Fixpoint subtrees t := if t is Fork t1 t2 then t :: subtrees t1 ++ subtrees t2 else t :: nil.

Theorem ex_6_1_3 t : size (subtrees t) = tsize t + nodes t.
  elim: t => // t P1 u P2. rewrite /subtrees -/subtrees /= size_cat P1 P2. ring.
Qed.

Fixpoint down n t := if t is Fork t1 t2 then Fork nat (down n.+1 t1) (down n.+1 t2) else Leaf nat n.
Definition depths := down 0.
Fixpoint maxBtree u :=
  match u with
    | Leaf x => x
    | Fork t1 t2 => maxn (maxBtree t1) (maxBtree t2)
  end.
Definition height t := maxBtree (depths t).

Lemma btree_gt_0 t : 0 < tsize t.
  elim: t => [a|t P u Q].
    - by rewrite /tsize.
    - rewrite /tsize -/tsize. apply ltn_addl. apply Q.
Qed.

Lemma maxn_plus m n p : maxn (p + m) (p + n) = p + maxn m n.
  elim: p => // p P. by rewrite 3!addSn maxnSS P.
Qed.

Lemma maxBtree_down n t : maxBtree (down n t) = n + height t.
  move: n. elim: t => [t|t P u Q n].
    - by rewrite /down /maxBtree /height /depths /down /maxBtree.
    - rewrite /down -/down /maxBtree -/maxBtree /height /depths /down -/down /maxBtree -/maxBtree.
      by rewrite 2!P 2!Q 2!maxn_plus addnA addn1.
Qed.

Lemma nltm_0ltp_Snltmp n m p : n < m -> 0 < p -> n.+1 < m + p.
  rewrite -{2}addn1. apply leq_add.
Qed.

Theorem ex_6_1_4 t : height t < tsize t.
  elim: t => //= => t P1 u P2.
  rewrite /height /depths /down -/down /maxBtree -/maxBtree 2!maxBtree_down maxnSS /maxn.
  elim: (height t < height u); [rewrite addnC|]; apply nltm_0ltp_Snltmp => //; apply btree_gt_0.
Qed.

Theorem ex_6_1_6 t : height t = maxBtree (depths t).
  by rewrite /depths maxBtree_down.
Qed.

