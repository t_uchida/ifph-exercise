Require Import ssreflect ssrnat ssrbool seq.

Class ord (A : Set) : Type := Ord {
  lt : A -> A -> bool;
  eq : A -> A -> bool;
  gt : A -> A -> bool;
  le : A -> A -> bool;
  ge : A -> A -> bool;
  ord_dec : forall x y, {lt x y} + {eq x y} + {gt x y};
  le_is_lt_or_eq : forall x y, le x y = lt x y || eq x y;
  ge_is_gt_or_eq : forall x y, ge x y = gt x y || eq x y
}.

Variable A : Set.
Variable O : ord A.
Implicit Type x y : A.

Inductive ordering x y : Set :=
  | Lt of lt x y : ordering x y
  | Eq of eq x y : ordering x y
  | Gt of gt x y : ordering x y.

Definition compare x y : ordering x y.
  elim (ord_dec x y); [elim|]; [apply Lt|apply Eq|apply Gt].
Qed.

Inductive stree : Set :=
  | SNull : stree
  | SFork : stree -> A -> stree -> stree.

Implicit Type t u v: stree.

Fixpoint flatten t := if t is SFork t1 y t2 then flatten t1 ++ y :: flatten t2 else nil.

Fixpoint member x t :=
  match t with
    | SNull => false
    | SFork t1 y t2 => match compare x y with
      | Lt _ => member x t1
      | Eq _ => true
      | Gt _ => member x t2
    end
  end.

Fixpoint height' t := if t is SFork t1 y t2 then S (maxn (height' t1) (height' t2)) else 0.

Fixpoint size' t := if t is SFork t1 y t2 then 1 + size' t1 + size' t2 else 0.

Definition power n m := nat_iter m (mult n) 1.

Lemma power2_Sn n : power 2 n.+1 = 2 * power 2 n.
  by elim: n.
Qed.

Lemma power2_monotone m n : power 2 m <= power 2 (m + n).
  elim: n => [|n P].
    - by rewrite addn0.
    - rewrite addnS. apply (leq_trans P). rewrite power2_Sn mul2n -addnn. apply leq_addr.
Qed.

Lemma power2_maxn_dist m n : power 2 (maxn m n) = maxn (power 2 m) (power 2 n).
  case/orP: (leq_total m n) => P.
    - by rewrite (maxn_idPr P) -{2}(subnKC P) (maxn_idPr (power2_monotone m (n - m))) (subnKC P).
    - by rewrite (maxn_idPl P) -{2}(subnKC P) (maxn_idPl (power2_monotone n (m - n))) (subnKC P).
Qed.

Theorem ex_6_2_1 t : height' t <= size' t < power 2 (height' t).
  apply/andP; split.
    - elim: t => //= t P x u Q. rewrite/maxn. elim: (height' t < height' u); rewrite -addnA add1n ltnS.
      + by apply/(@leq_trans (size' u))/leq_addl.
      + by apply/(@leq_trans (size' t))/leq_addr.
    - elim: t => //= t P x u Q. rewrite power2_Sn.
      apply (@leq_trans (power 2 (height' t) + power 2 (height' u))).
      +  rewrite -addnA add1n. apply (@leq_ltn_trans (size' t + power 2 (height' u))).
        - by rewrite ltn_add2l.
        - by rewrite ltn_add2r.
      + rewrite mul2n -addnn power2_maxn_dist. apply leq_add.
        - apply leq_maxl.
        - apply leq_maxr.
Qed.

Fixpoint insert x t :=
  match t with
    | SNull => SFork SNull x SNull
    | SFork t1 y t2 =>
        match compare x y with
          | Lt _ => SFork (insert x t1) y t2
          | Eq _ => SFork t1 y t2
          | Gt _ => SFork t1 y (insert x t2)
        end
  end.

Inductive locally_sorted op : seq A -> Prop :=
  | LSNil : locally_sorted op [::]
  | LSCons1 x : locally_sorted op [::x]
  | LSConsN x y s : locally_sorted op (y :: s) -> op x y -> locally_sorted op (x :: y :: s).

Definition inordered t := locally_sorted le (flatten t).

Lemma locally_sorted_cons op x s : locally_sorted op (x :: s) -> locally_sorted op s.
  admit.
Qed.

Lemma locally_sorted_app op (s1 s2 : seq A) : locally_sorted op (s1 ++ s2) -> locally_sorted op s1 /\ locally_sorted op s2.
  admit.
Qed.

Lemma locally_sorted_fork t u x : inordered (SFork t x u) -> inordered t /\ inordered u.
  admit.
Qed.

Lemma locally_sorted_join_l t u x y : inordered t -> inordered u -> le x y -> inordered (SFork (insert x t) y u).
  admit.
Qed.

Lemma locally_sorted_join_r t u x y : inordered t -> inordered u -> gt x y -> inordered (SFork t y (insert x u)).
  admit.
Qed.

Lemma lt_le x y : lt x y -> le x y.
  move=> P. rewrite le_is_lt_or_eq. by rewrite P.
Qed.

Theorem ex_6_2_4 t x : inordered t -> inordered (insert x t).
  move: x. elim: t => [P Q|t P y u Q z R]. 
    - rewrite /insert /inordered /flatten => /=. apply LSCons1.
    - rewrite /insert -/insert. elim (compare z y) => S //;
      move: (locally_sorted_fork t u y R) => [T U].
      + move: (P z T) => V. apply/locally_sorted_join_l/lt_le => //.
      + move: (Q z U) => V. apply/locally_sorted_join_r => //.
Qed.

Fixpoint join t u :=
  match t with
    | SNull => u 
    | SFork t1 x t2 => SFork t1 x (join t2 u)
  end.

Fixpoint delete x t :=
  match t with
    | SNull => SNull
    | SFork t1 y t2 =>
        match compare x y with
          | Lt _ => SFork (delete x t1) y t2
          | Eq _ => join t1 t2
          | Gt _ => SFork t1 y (delete x t2)
        end
  end.

Definition headTree t := head (flatten t).

Definition empty t := if t is SNull then true else false.

Fixpoint splitTree' t :=
  match t with
  | SNull => (None, SNull)
  | SFork t1 y t2 =>
      match empty t1 with
      | true => (Some y, t2)
      | false => let (x, wt) := splitTree' t1 in (x, SFork wt y t2)
      end
  end.

Theorem ex_6_2_5 t u m n : m = height' t -> n = height' u -> maxn m n <= height' (join t u) <= maxn m n + 1.
Abort.

(* 
Theorem ex_6_2_6 : forall A `(Ord A) (t : Stree A), splitTree t = splitTree' t.
  admit.
Qed.
*)

