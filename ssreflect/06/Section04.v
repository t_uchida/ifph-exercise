Require Import ssreflect seq ssrnat ssrbool.

Set Implicit Arguments.

Class ord (A : Set) : Type := Ord {
  gle : A -> A -> bool;
  zero : A
}.

Section rose_tree.

Definition max {A} `{ord A} x y := if gle x y then y else x.

Inductive rose A := Node : A -> seq (rose A) -> rose A.

Definition sum := foldl plus 0.
Fixpoint size {A} `{ord A} (r : rose A) := match r with Node x s => 1 + sum (map size s) end.
Definition maxlist {A} `{ord A} := foldl max zero.
Fixpoint down {A} `{ord A} (n : nat) (r : rose A) := match r with Node _ s => Node n (map (down n.+1) s) end.
Definition depths {A} `{ord A} := down 1.
Fixpoint max_rose {A} `{ord A} (r : rose A) := match r with Node x s => max x (maxlist (map max_rose s)) end.

End rose_tree.

Instance ord_nat : ord nat := Ord leq 0.

Fixpoint height {A} `{ord A} (rn : rose A) := match rn with Node _ ns => 1 + maxlist (map height ns) end.

Lemma max_rose_Sn x s n : max_rose (down n.+1 (Node x s)) = 1 + max_rose (down n (Node x s)).
  admit.
Qed.

Lemma maxlist_max_rose x s : maxlist (map height s) = max_rose (down 0 (Node x s)).
  admit.
Qed.

Theorem ex_6_4_1 r : height r = max_rose (depths r).
  elim: r => x s. by rewrite /height -/height /depths max_rose_Sn -maxlist_max_rose.
Qed.

