Require Import
  Coq.Strings.String Coq.Strings.Ascii
  Ssreflect.ssreflect Ssreflect.ssrfun Ssreflect.ssrbool Ssreflect.eqtype Ssreflect.ssrnat Ssreflect.seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition beq x y := if ascii_dec x y is left _ then true else false.

Definition concat {A} (ss : seq (seq A)) := foldl cat nil ss.

Definition codetable := seq (ascii * seq bool).

Inductive Btree A :=
  | Leaf : A -> Btree A
  | Fork : Btree A -> Btree A -> Btree A.

Implicit Type x y : ascii.
Implicit Type c d : codetable.
Implicit Type t u : Btree ascii.

Fixpoint lookup c x :=
  match c with
    | nil => None
    | (y, bs) :: c' => if beq x y then Some bs else lookup c' y
  end.

Fixpoint hufmerge c d :=
  match c with
    | nil => map (fun z => match z with (y, cs) => (y, true :: cs) end) d
    | (x, bs) :: c' =>
      (fix hufmerge' d' :=
        match d' with
          | nil => map (fun z => match z with (x, bs) => (x, false :: bs) end) c
          | (y, cs) :: d'' =>
            if size bs <= size cs
              then (x, false :: bs) :: hufmerge c' d'
              else (y, true :: cs) :: hufmerge' d''
        end) d
  end.

Fixpoint transform t :=
  match t with
    | Leaf x => (x, nil) :: nil
    | Fork t1 t2 => hufmerge (transform t1) (transform t2)
  end.

Fixpoint filter_some {A} (s : seq (option A)) :=
  match s with
    | nil => nil
    | Some z :: s' => z :: filter_some s'
    | _ :: s' => filter_some s'
  end.

Definition encode t s := concat (filter_some (map (lookup (transform t)) s)).

Local Open Scope string_scope.

(*
Goal forall (c : string), True.
  move=> c. done. elim: c.
Goal forall (c : ascii), True.
  move=> c. elim: c.
*)

